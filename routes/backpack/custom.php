<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace' => 'App\Http\Controllers\Admin',
], function () {
    CRUD::resource('finances', 'FinancesCrudController');
    CRUD::resource('works', 'WorksCrudController');
    CRUD::resource('schools', 'SchoolsCrudController');
    CRUD::resource('movies', 'MoviesCrudController');
    CRUD::resource('accounts', 'AccountsCrudController');
    CRUD::resource('gadget', 'GadgetCrudController');
    CRUD::resource('house', 'HouseCrudController');
    CRUD::resource('notes', 'NotesCrudController');
    CRUD::resource('photos', 'PhotosCrudController');
    CRUD::resource('others', 'OthersCrudController');
    CRUD::resource('logs', 'LogsCrudController');
});
