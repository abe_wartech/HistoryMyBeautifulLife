<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class LoginSeeder extends Seeder
{
    public function run()
    {
        $menu = array(
            array('ABE','abe@abe.com',Hash::make('123456')),
            );
    
            $menucount = count($menu);
            
            for ($i=0; $i < $menucount; $i++) {
                DB::table('users')->insert(array(
                'name' => $menu[$i][0],
                'email' => $menu[$i][1],
                'password' => $menu[$i][2],
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                ));
            }
    }
}
