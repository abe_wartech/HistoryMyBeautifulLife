<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('finances') }}"><i class="fa fa-bank"></i> <span>Finances</span></a></li>
<li><a href="{{ backpack_url('works') }}"><i class="fa fa-laptop"></i> <span>Works</span></a></li>
<li><a href="{{ backpack_url('schools') }}"><i class="fa fa-graduation-cap"></i> <span>Schools & Collage</span></a></li>
<li><a href="{{ backpack_url('movies') }}"><i class="fa fa-film"></i> <span>Movies & Tv Series</span></a></li>
<li><a href="{{ backpack_url('accounts') }}"><i class="fa fa-facebook-official"></i> <span>Accounts & Password</span></a></li>
<li><a href="{{ backpack_url('gadget') }}"><i class="fa fa-android"></i> <span>Gadget</span></a></li>
<li><a href="{{ backpack_url('house') }}"><i class="fa fa-home"></i> <span>House</span></a></li>
<li><a href="{{ backpack_url('notes') }}"><i class="fa fa-sticky-note-o"></i> <span>Notes</span></a></li>
<li><a href="{{ backpack_url('photos') }}"><i class="fa fa-image"></i> <span>Photos</span></a></li>
<li><a href="{{ backpack_url('others') }}"><i class="fa fa-th-list"></i> <span>Others</span></a></li>
<li><a href='{{ url(config('backpack.base.route_prefix', 'admin').'/backup') }}'><i class='fa fa-hdd-o'></i> <span>Backups</span></a></li>
<li><a href="{{ backpack_url('logs') }}"><i class="fa fa-terminal"></i> <span>Logs</span></a></li>
