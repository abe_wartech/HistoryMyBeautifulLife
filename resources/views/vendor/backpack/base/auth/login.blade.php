@extends('backpack::loginlayout') @section('content')

<div class="login-box">
    <div class="login-logo">
        <img src="{{ asset('img/abe2.png')}}" style="width: 350px; height: 110px;">

    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><b>ABE SOFT</b><br><span class="badge bg-blue">0.0.1-12May2018</span></p>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/login') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                <input placeholder="E-mail" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"> @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span> @endif
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                <input placeholder="Password" id="password" type="password" class="form-control" name="password"> @if ($errors->has('password'))
                <span class="help-block ">
                    <strong>{{ $errors->first('password') }}</strong>
                </span> @endif
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Log in</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection