@extends('backpack::layout') @section('header')
<section class="content-header">
  <h1>

  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a>
    </li>
    <li class="active">{{ trans('backpack::base.dashboard') }}</li>
  </ol>
</section>
@endsection @section('content')
<div class="row">
  <div class="col-md-4">
    <div class="small-box" style="background-color:#F03434;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">FINANCES</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-cash" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('finances') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus Users
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#F22613;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">WORKS</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-briefcase" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('works') }}" class="small-box-footer" style="color:white;">Grafik Telpon Mystery Call
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#F62459;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">SCHOOLS & COLLAGE</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-university" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('schools') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#DB0A5B;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">MOVIES & TV SERIES</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-film-marker" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('movies') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#913D88;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">ACCOUNTS & PASSWORD</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-social-facebook" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('accounts') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#446CB3;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">GADGET</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-iphone" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('gadget') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#2ABB9B;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">HOUSE</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-home" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('house') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#22A7F0;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">NOTES</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-android-calendar" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('notes') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#6BB9F0;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">PHOTOS</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-images" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('photos') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#F89406;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">OTHERS</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-podium" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('others') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#00E640;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">BACKUPS</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-android-archive" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('backup') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="small-box" style="background-color:#00B16A;">
      <div class="inner" style="margin-bottom:30px;">
        <h4>
          <B style="color:white;">LOGS</B>
        </h4>
      </div>
      <div class="icon">
        <i class="ion ion-refresh" style="font-size:80px;"></i>
      </div>
      <a href="{{ backpack_url('logs') }}" class="small-box-footer" style="color:white;">Ubah/Tambah/Hapus CABANG
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
</div>
@endsection